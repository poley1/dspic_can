/* Copyright 2018 The MathWorks, Inc. */
#ifndef _MY_FUNC_H_
#define _MY_FUNC_H_
#include <stdint.h>
#include "C:\Program Files\Microchip\xc16\v1.50\support\dsPIC33E\h\p33EV256GM106.h" // Located in XC16 compiler folder 
// Have deleted processor check in p33 INCLUDE above as it was calling as Error!!!!!

// CAN Error Codes used by this driver
// 0 is OK, non-zero is some kind of error
typedef enum 
{
   CAN_EC_OK = 0,
   CAN_EC_BUFFER_RX_EMPTY,    //can_getd failed because all RX buffers are empty, or specified buffer is empty
   CAN_EC_BUFFER_TX_FULL,     //can_putd failed because all TX buffers are full, or specified buffer is full
   CAN_EC_BUFFER_NOT_TX,      //can_putd failed because buffer is not a TX buffer
   CAN_EC_BUFFER_IS_RTR,      //can_putd failed because buffer is setup as an RTR TX Buffer
   CAN_EC_BUFFER_NO_RTR,      //CAN Buffer can not be set to respond to RTR requests
   CAN_EC_BAUD_NOT_DIVISIBLE, //can_set_baud failed because clock not divisible by baud rate
   CAN_EC_BAUD_INVALID,       //can_set_baud failed because baud rate not achievable
} can_ec_t;

// CAN TX Message Header type
typedef struct
{
   uint32_t Id;               //ID to send message with
   uint8_t Length;            //Number of data bytes to send
   uint8_t ext;                  //TRUE - send message with extended ID, FALSE - send message with standard ID
   uint8_t rtr;                  //TRUE - send RTR (request), FALSE - not an RTR (request)
   uint8_t Priority:2;        //0-3, use to determine which messages will be sent fist.  The high the priority the sooner the ECAN peripheral will send the message
} CAN_TX_HEADER;

// CAN RX Message Header type
typedef struct
{
   uint32_t Id;               //ID of received message
   uint8_t Length;            //Number of data bytes received
   uint8_t Filter;            //Filter message was received with
   uint8_t Buffer;            //Buffer message was retreived from
   uint8_t err_ovfl;             //TRUE - Buffer overflowed, FALSE - Buffer didn't overflow
   uint8_t ext;                  //TRUE - received extended ID message, FALSE - received standard ID message
   uint8_t rtr;                  //TRUE - received RTR (request), FALSE - not a RTR (request)
} CAN_RX_HEADER;

// CAN_OP_MODE type
typedef enum 
{
   CAN_OP_NORMAL,             //Normal operation mode
   CAN_OP_DISABLE,            //Disable mode
   CAN_OP_LOOPBACK,           //Loopback mode
   CAN_OP_LISTEN,             //Listen only mode
   CAN_OP_CONFIG,             //Configuration mode
   CAN_OP_LISTEN_ALL=7        //Listen all messages mode
} CAN_OP_MODE;

// CAN_FILTER_MASK type
typedef enum
{
   CAN_FILTER_MASK_0,
   CAN_FILTER_MASK_1,
   CAN_FILTER_MASK_2
} CAN_FILTER_MASK;

// CAN_MASK_ID_TYPE type
typedef enum {
   CAN_MASK_ID_TYPE_SID,   //Mask Id is a SID Id - only SID bits of Mask are written, EID bits are set to 0
   CAN_MASK_ID_TYPE_EID    //Mask Id is an EID Id - SID and EID bits of Mask are written
} CAN_MASK_ID_TYPE;

// CAN_FILTER_MASK_TYPE type
typedef enum {
   CAN_FILTER_MASK_TYPE_EITHER,     //match either SID or EID messages, ignores EXIDE bit of filter
   CAN_FILTER_MASK_TYPE_SID_OR_EID  //only match SID or EID messages, EXIDE bit of filter determines type of messages received
} CAN_FILTER_MASK_TYPE;

// CAN_FILTER type
typedef enum {CAN_FILTER_0, CAN_FILTER_1, CAN_FILTER_2, CAN_FILTER_3, 
              CAN_FILTER_4, CAN_FILTER_5, CAN_FILTER_6, CAN_FILTER_7,
              CAN_FILTER_8, CAN_FILTER_9, CAN_FILTER_10, CAN_FILTER_11,
              CAN_FILTER_12, CAN_FILTER_13, CAN_FILTER_14, CAN_FILTER_15
} CAN_FILTER;

// CAN_FILTER_TYPE type
typedef enum {
   CAN_FILTER_TYPE_SID,    //Filter Id is a SID Id - only SID bits of filter are written, EID bits are set to 0
   CAN_FILTER_TYPE_EID     //Filter Id is an EID Id - SID and EID bits of Mask are written
} CAN_FILTER_TYPE;

// CAN_BUFFER type
typedef enum { CAN_BUFFER_0, CAN_BUFFER_1, CAN_BUFFER_2, CAN_BUFFER_3, CAN_BUFFER_4, CAN_BUFFER_5, CAN_BUFFER_6, CAN_BUFFER_7,
               CAN_BUFFER_8, CAN_BUFFER_9, CAN_BUFFER_10, CAN_BUFFER_11, CAN_BUFFER_12, CAN_BUFFER_13, CAN_BUFFER_14, CAN_BUFFER_15,
               CAN_BUFFER_16, CAN_BUFFER_17, CAN_BUFFER_18, CAN_BUFFER_19, CAN_BUFFER_20, CAN_BUFFER_21, CAN_BUFFER_22, CAN_BUFFER_23,
               CAN_BUFFER_24, CAN_BUFFER_25, CAN_BUFFER_26, CAN_BUFFER_27, CAN_BUFFER_28, CAN_BUFFER_29, CAN_BUFFER_30, CAN_BUFFER_31
} CAN_BUFFER;

// CAN_FILTER_BUFFER type
typedef enum {CAN_FILTER_BUFFER_0, CAN_FILTER_BUFFER_1, CAN_FILTER_BUFFER_2, CAN_FILTER_BUFFER_3,
              CAN_FILTER_BUFFER_4, CAN_FILTER_BUFFER_5, CAN_FILTER_BUFFER_6, CAN_FILTER_BUFFER_7,
              CAN_FILTER_BUFFER_8, CAN_FILTER_BUFFER_9, CAN_FILTER_BUFFER_10, CAN_FILTER_BUFFER_11,
              CAN_FILTER_BUFFER_12, CAN_FILTER_BUFFER_13, CAN_FILTER_BUFFER_14, CAN_FILTER_BUFFER_FIFO              
} CAN_FILTER_BUFFER;

// CAN_INTERRUPT type
typedef enum
{
   CAN_INTERRUPT_TX = 0x01,         //TX Buffer Interrupt
   CAN_INTERRUPT_RX = 0x02,         //RX Buffer Interrupt
   CAN_INTERRUPT_RXOV = 0x04,       //RX Buffer Overflow Interrupt
   CAN_INTERRUPT_FIFO = 0x08,       //FIFO Almost Full Interrupt
   CAN_INTERRUPT_ERR = 0x20,        //Error Interrupt
   CAN_INTERRUPT_WAKE = 0x40,       //Bus Wake-up Activity Interrupt
   CAN_INTERRUPT_INVALID = 0x80     //Invalid Message Interrupt
} CAN_INTERRUPT;

/* custom c functions */
// extern double add(double u1, double u2);
// extern double timesK(double u, double K);
extern void can_init(void);

#endif