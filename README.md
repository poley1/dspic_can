Please delete .slx files leaving the one that matches your Matlab version - If you need it saved as any other version then please let me know!

Must haves:
1. XC16 Compiler downloaded from MPLAB - https://www.microchip.com/mplab/compilers - The path from my_func.h includes p33EV256GM106.h from the 'Support' Folder in the XC16 dir.
2. MPLAB Simulink blocks must be installed - https://uk.mathworks.com/matlabcentral/fileexchange/71892-mplab-device-blocks-for-simulink-dspic-pic32-and-sam-mcu
3. Simulink and Embedded Coder installed

Chnages made:
I have removed the processor check at the top of the p33EV256GM106.h file as it kept giving me errors, if anyone can see the issue for that it would be great!