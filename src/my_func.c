/* Copyright 2018 The MathWorks, Inc. */
#ifndef __dsPIC33EV256GM106__
#define __dsPIC33EV256GM106__
#endif

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "tmwtypes.h"
#include "my_func.h"

// Buffer for CAN1
uint16_t ecan1_message_buffer[32][8];
uint8_t ecan1_last_fifo_buffer = (31);

void can_init(void) {
    // can_set_mode(CAN_OP_CONFIG, TRUE);   //must be in config mode before params can be set
    C1CTRL1bits.REQOP = CAN_OP_CONFIG;
//        while (C1CTRL1bits.OPMODE != CAN_OP_CONFIG);

//    C1CTRL1bits.CANCKS = 1;

//    C1CFG1 = 0x000F; // Set up registers for 500k on this 8Mhz crystal
//    C1CFG2 = 0x01B5;

//    C1FCTRLbits.DMABS = 6;

    // Configure the DMA
//    DMA0REQ = 0x0046;
//    DMA0STAL = (uint16_t) & ecan1_message_buffer[0][0];
//    DMA0STBL = (uint16_t) & ecan1_message_buffer[0][0];
//    DMA0PAD = 0x0442;
//    DMA0CNT = 0x0007;
//    DMA0CON = 0xA020;
//    DMA1REQ = 0x0022;
//    DMA1STAL = (uint16_t) & ecan1_message_buffer[0][0];
//    DMA1STBL = (uint16_t) & ecan1_message_buffer[0][0];
//    DMA1PAD = 0x0440;
//    DMA1CNT = 0x0007;
//    DMA1CON = 0xA020;


    //Enable Transmit Buffers
//    C1TR01CONbits.TXEN0 = 1;
//    C1FCTRLbits.FSA = 1;

//    C1FEN1 = 0; //Disable all filters
    /*
    //Setup Filters
    can_set_mask_id(CAN_FILTER_MASK_0, CAN_USER_MASK_0, CAN_USER_MASK_0_ID_TYPE, CAN_USER_MASK_0_FILTER_TYPE);
    can_set_filter_id(CAN_FILTER_0, CAN_USER_FILT_0, CAN_USER_FILT_0_TYPE);
    can_enable_filter(CAN_FILTER_0, CAN_USER_FILT_0_BUFFER, CAN_USER_FILT_0_MASK);
     */
//    C1CTRL1bits.REQOP = CAN_OP_NORMAL;
//    while (C1CTRL1bits.OPMODE != CAN_OP_NORMAL);
}